const isTest = process.env['NODE_ENV'] === 'test';
const ignore = isTest ? [] : ['src/**/*.test.js'];

module.exports = function (api) {
  api.cache(true);

  return {
    presets: [
      '@babel/preset-react', 
      '@babel/preset-env'
    ],
    plugins: [
      '@babel/plugin-proposal-function-bind',
      '@babel/plugin-proposal-class-properties',
      ['@babel/plugin-proposal-decorators', { 'decoratorsBeforeExport': false }],
      ['module-resolver', {
        'root': ['./src'],
        'extensions': ['.js', '.jsx'],
        'alias': {
          'img': './img'
        }
      }]
    ],
    ignore
  };
}
