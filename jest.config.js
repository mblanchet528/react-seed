module.exports = {
  clearMocks: true,
  testMatch: [
    '**/src/**/*.test.js',
    '**/src/**/*.test.jsx'
  ],
  setupFiles: [
    'raf/polyfill',
    '<rootDir>/enzyme.config.js',
    '<rootDir>/mocks/setupMocks.js'
  ],
  moduleNameMapper: {
    '\\.(css|less)$': 'identity-obj-proxy'
  },
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/jest.fileTransformer.js'
  },
  snapshotSerializers: [
    "enzyme-to-json/serializer"
  ],
  globals: {
    __DEV__: false
  }
}