import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { createGlobalStyle } from 'styled-components';

export default class App extends Component {
  static propTypes = {};

  render() {
    const GlobalStyle = createGlobalStyle`
      * {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
      }
    `;

    return (
      <div>
        <GlobalStyle />
        <div>My Wonderful React Application</div>
      </div>
    );
  }
}