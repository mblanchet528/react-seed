import React from 'react';
import { shallow } from 'enzyme';

import App from 'components/app';

describe('App', () => {
  let wrapper;
  const app = () => {
    if (!wrapper) {
      wrapper = shallow(<App/>);
    }
    return wrapper;
  };

  beforeEach(() => {
    wrapper = undefined;
  });

  it('should render properly', () => {
    expect(app()).toMatchSnapshot();
  });
});