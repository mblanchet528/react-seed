import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Provider } from 'react-redux';

import store from 'store';
import App from './index';

export default class Root extends Component {
  static propTypes = {};

  render() {
    return (
      <Provider store={store}>
        <App/>
      </Provider>
    );
  }
}
