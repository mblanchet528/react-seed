import React from 'react';
import ReactDOM from 'react-dom';

import Root from 'components/app/root';

ReactDOM.render(<Root />, document.getElementById('root'));
