let enhancers;
if (__DEV__) {
  enhancers = require('./storeEnhancers.dev').default;
} else {
  enhancers = require('./storeEnhancers.prod').default;
}

export default enhancers.filter(e => !!e);