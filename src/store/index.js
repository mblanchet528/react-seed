import React from 'react';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import storeEnhancers from './enhancers/storeEnhancers';

const enhancers = compose(
  applyMiddleware(thunk),
  ...storeEnhancers
);

export default createStore(() => ({}), undefined, enhancers);