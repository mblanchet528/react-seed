var webpack = require('webpack');
var path = require('path');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var config = require('config');
var fs = require('fs');

var SRC_DIR = path.resolve(__dirname, 'src');

var CONFIG_OUT_PATH = 'public/config.json';
fs.writeFileSync(path.resolve(__dirname, CONFIG_OUT_PATH), JSON.stringify(config));

module.exports = {
  entry: path.resolve(SRC_DIR, 'index.jsx'),
  output: {
    path: path.resolve(__dirname, 'public'),
    publicPath: '/',
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.jsx?/,
        include: [SRC_DIR],
        loader: 'babel-loader',
        query: {
          presets: [
            '@babel/preset-react', 
            '@babel/preset-env'
          ],
          plugins: [
            '@babel/plugin-proposal-function-bind',
            '@babel/plugin-proposal-class-properties',
            ['@babel/plugin-proposal-decorators', { 'decoratorsBeforeExport': false }]
          ]
        }
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: {
          loader: 'file-loader',
          options: {
            outputPath: 'img/'
          }
        }
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      __DEV__: JSON.stringify(config.has('dev') ? config.get('dev') : false)
    }),
    new CopyWebpackPlugin([
      {from: 'node_modules/bootstrap/dist/css/bootstrap-grid.min.css', to: 'style/vendor/bootstrap-grid.min.css'},
      {from: 'node_modules/animate.css/animate.css', to: 'style/vendor/animate.css'},
      {from: 'node_modules/font-awesome/css/font-awesome.min.css', to: 'fonts/assets/font-awesome/css/font-awesome.min.css'},
      {from: 'node_modules/font-awesome/fonts', to: 'fonts/assets/font-awesome/fonts'},
    ])
  ],
  resolve: {
    extensions: ['.js', '.jsx', '.css'],
    alias: {
      configuration: path.resolve(__dirname, CONFIG_OUT_PATH),
      style: path.resolve(__dirname, 'style/')
    }
  },
  devServer: {
    contentBase: './public',
    historyApiFallback: true,
    inline: true,
    open: true
  }
};
